package com.founder.service;

import com.founder.core.domain.RefundOrder;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IRefundOrderService {

    RefundOrder selectRefundOrder(String refundOrderId);

    Page<RefundOrder> selectRefundOrderList(int offset, int limit, RefundOrder refundOrder);

    @Deprecated
    List<RefundOrder> getRefundOrderList(int offset, int limit, RefundOrder refundOrder);

    @Deprecated
    Integer count(RefundOrder refundOrder);
}
