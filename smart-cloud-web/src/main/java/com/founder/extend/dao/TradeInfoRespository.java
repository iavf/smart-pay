package com.founder.extend.dao;

import com.founder.extend.domain.TradeInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
@Repository
public interface TradeInfoRespository extends JpaRepository<TradeInfo, String> {
}
