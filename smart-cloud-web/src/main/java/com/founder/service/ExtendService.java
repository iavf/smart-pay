package com.founder.service;

import com.founder.core.constant.PayConstant;
import com.founder.core.dao.PayOrderRespository;
import com.founder.core.domain.PayOrder;
import com.founder.core.log.MyLog;
import com.founder.extend.dao.OrderInfoRespository;
import com.founder.extend.domain.OrderInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@Service
public class ExtendService {

    private final MyLog _log = MyLog.getLog(ExtendService.class);

    @Autowired
    PayOrderRespository payOrderRespository;

    @Autowired
    OrderInfoRespository orderInfoRespository;

    /**
     *
     * @param orderNo
     * @return
     */
    PayOrder trans(String mchId, String orderNo){
        PayOrder payOrder = null;
        if (!payOrderRespository.existsById(orderNo)){
            _log.info("查询分库订单数据");
            Optional<OrderInfo> optional = orderInfoRespository.findById(orderNo);
            if (!optional.isPresent()){
                return null;
            }

            OrderInfo orderInfo = optional.get();
            payOrder = new PayOrder();
            payOrder.setMchId(mchId);
            payOrder.setPayOrderId(orderInfo.getOrder_no());
            payOrder.setMchOrderNo(orderNo);
            payOrder.setChannelId("alipay.precreate".equalsIgnoreCase(orderInfo.getScene()) ? "ALIPAY_QR" : "WX_NATIVE");
            payOrder.setAmount(orderInfo.getTotal_amount().multiply(new BigDecimal(100)).longValue());
            payOrder.setCurrency("cny");
            payOrder.setClientIp(orderInfo.getClient_ip());
            payOrder.setDevice(orderInfo.getClient_ip());
            payOrder.setSubject(orderInfo.getSubject());
            payOrder.setBody(StringUtils.isBlank(orderInfo.getSubject()) ? "" : orderInfo.getSubject());
            payOrder.setExtra(orderInfo.getAuth_code());
            payOrder.setChannelMchId("alipay.precreate".equalsIgnoreCase(orderInfo.getScene()) ? "20886217854529070156" : "1246109501");
            payOrder.setChannelOrderNo(orderInfo.getTrade_no());
            payOrder.setErrCode("");
            payOrder.setErrMsg("补录");
            payOrder.setParam1(orderInfo.getBuyer_user_id());
            payOrder.setParam2(orderInfo.getBuyer_logon_id());
            payOrder.setNotifyUrl("");
            payOrder.setNotifyCount(0);
            payOrder.setPaySuccTime(System.currentTimeMillis());
            payOrder.setCreateTime(orderInfo.getOrder_date());//取分库订单时间
            payOrder.setUpdateTime(new Date());
            if ("1".equalsIgnoreCase(orderInfo.getTrade_status())){//0:未支付 1:支付成功 2:等待用户支付 3: 支付失败 4:部分退款 5:全部退款
                payOrder.setStatus(PayConstant.PAY_STATUS_SUCCESS);//从分库转换
            } else if ("3".equalsIgnoreCase(orderInfo.getTrade_status())){
                payOrder.setStatus(PayConstant.PAY_STATUS_FAILED);
            } else {
                payOrder.setStatus(PayConstant.PAY_STATUS_PAYING);
            }

            payOrderRespository.save(payOrder);
        } else {
            Optional<PayOrder> optional = payOrderRespository.findById(orderNo);
            payOrder = optional.get();
        }

        return payOrder;
    }
}
